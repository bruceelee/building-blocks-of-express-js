var express = require('express')
var app = express()
 
app.get('/blocks', function (req, res) {
 	res.redirect(301, '/parts');  //Redirigiendo a /parts con un status code, en este caso mover permanentemente 	
});
 
app.listen(3000,function(){
	console.log('Listening to port 3000');
});	