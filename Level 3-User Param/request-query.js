//request.query permite acceder al string de query

var express = require('express')
var app = express()

app.get('/blocks',function(request, response){
	var blocks= ['Fixed','Movable','Rotating'];
	if(request.query.limit >=0){ //Se cumple cuando existe un valor limit en la URL
		response.json(blocks.slice(0,request.query.limit));
	} else {		//Retorna todos los resultados
	response.json(blocks);  
	}
});
 
app.listen(3000);	