//Este ejemplo se aplica todo lo aprendido en este nivel

var express = require('express')
var app = express()

var blocks={
	'Fixed':'Fastened securely in position',
	'Movable': 'Capable of being moved',
	'Rotating':'Moving in a circle around'
};

var locations={
	'Fixed':'First Floor',
	'Movable': 'Second Floor',
	'Rotating':'Penthouse'
};

app.use(express.static('public'));

//Usando un middleware para manejar las duplicaciones en 'name'
app.param('name',function(request, response, next){ 
	var name= request.params.name;
	var block= name[0].toUpperCase()+ name.slice(1).toLowerCase();

	request.blockName=block; //Este valor puede ser accedido por otras routes en la aplicacion
	next(); //Paso para continuar el request

});

app.get('/blocks',function(request, response){
	response.json(Object.keys(blocks));
});

app.get('/blocks/:name',function(request, response){
	var description= blocks[request.blockName];  //Ahora puedo acceder directamente

	if(!description){
		response.status(404).json('No description found for '+ request.blockName);
	}
	else{
		response.json(description);
	}
});
 

 app.get('/locations/:name',function(request, response){
	var location= locations[request.blockName];  //Ahora puedo acceder directamente

	if(!location){
		response.status(404).json('No description found for '+ request.blockName);
	}
	else{
		response.json(location);
	}
});

app.listen(3000);	