module.exports= function(request, response, next){
	var start= +new Date(); //Plus sign convert date Object to milliseconds
	var stream= process.stdout;
	var url= request.url;
	var method= request.method;

//Event handler functions runs asynchronously. El evento 'finish' se emite
//cuando una respuesta ha finalizado.
	response.on('finish', function(){		
		var duration= +new Date -start;
		var message= method + ' to '+ url +
		'\ntook' + duration + 'ms \n\n';
		//Imprimiemos el mesage
		stream.write(message);
	});

	next(); //Salta al siguiente middleware
}