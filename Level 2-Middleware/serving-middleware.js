var express = require('express')
var app = express()
 
/*express.static es equivalente a lo siguiente, o sea, a mandar el archivo index en public
  app.get('/', function (request, response) {
  response.sendFile(__dirname + '/public/index.html');
});*/ 
app.use(express.static('public-logger'));

app.get('/blocks',function(request, response){
	var blocks= ['Fixed','Movable','Rotating'];
	response.json(blocks);
});
 
app.listen(3000);	