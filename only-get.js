//Let's build a middleware that ensures only GET requests are allowed to go through.

module.exports= function(request, response, next){
  var method= request.method;
  if (method== 'GET')
  	next();
  else
    response.send('Method is not allowed');
};