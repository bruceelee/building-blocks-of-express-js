//Agrego dependencia de body-parser
//Las rutas pueden tener 'múltiples controladres'como argumentos y deben ser llamados secuencialmente

var express = require('express');
var app = express();

var bodyParser= require('body-parser'); //A new body object containing the parsed data is populated on the request object after the middleware (i.e. req.body).
var parseUrlencoded= bodyParser.urlencoded({extended:false}); //Forza el uso la libreria node nativa 'querystring', 

var blocks={
	'Fixed':'Fastened securely in position',
	'Movable': 'Capable of being moved',
	'Rotating':'Moving in a circle around'
};

var locations={
	'Fixed':'First Floor',
	'Movable': 'Second Floor',
	'Rotating':'Penthouse'
};

app.use(express.static('public-delete'));

/****************************DELETE********************************/

app.delete('/blocks/:name',function(request,response){
	if(blocks[request.blockName]){
		delete blocks[request.blockName];  //que corresponde a app.param('name',...)
		response.sendStatus(200);
	}
	else
		response.sendStatus(404);
	});

/****************************FIN DELETE********************************/



/****************************POST********************************/
//Las rutas pueden tener 'múltiples controladores' como argumentos y deben ser llamados secuencialmente
//Agrego ruta para POST blocks. Primero se ejecuta parseUrlencoded y luego la funcion anonima
//Poder manejar multiples rutas es util para re-utilizar middleware que carge recursos, produzca validaciones
// autentifacion, etc
app.post('/blocks',parseUrlencoded,function(request,response){
	//La informacion contenidad en el 'Form' puede ser accesida a traves de request.body
	var newBlock= request.body;  //retorna la data en Form
	blocks[newBlock.name]= newBlock.description; //Se crea nuevo bloque

	response.status(201).json(newBlock.name);  //Se responde con el nombre del nuevo block y status 201
});

/****************************FIN POST********************************/


//Usando un middleware para manejar las duplicaciones en 'name'
app.param('name',function(request, response, next){ 
	var name= request.params.name;
	var block= name[0].toUpperCase()+ name.slice(1).toLowerCase();

	request.blockName=block; //Este valor puede ser accedido por otras routes en la aplicacion
	next(); //Paso para continuar el request

});

app.get('/blocks',function(request, response){
	response.json(Object.keys(blocks));
});

app.get('/blocks/:name',function(request, response){
	var description= blocks[request.blockName];  //Ahora puedo acceder directamente

	if(!description){
		response.status(404).json('No description found for '+ request.blockName);
	}
	else{
		response.json(description);
	}
});
 

 app.get('/locations/:name',function(request, response){
	var location= locations[request.blockName];  //Ahora puedo acceder directamente

	if(!location){
		response.status(404).json('No description found for '+ request.blockName);
	}
	else{
		response.json(location);
	}
});

app.listen(3000);	