/*
4.3 Validation 

The way that it is now, we are allowing new cities to be created with a blank description. Let's add some validation so that in order for a city to be created, its description must have a string length greater than 4.

*/

var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var parseUrlencoded = bodyParser.urlencoded({ extended: false });

app.post('/cities', parseUrlencoded, function (request, response) {
 
  	
  if(request.body.description.length >4){
    var city = createCity(request.body.name, request.body.description);	
    response.status(201).json(city);
  } 
  else{
  	response.status(400).json('Invalid City');
  }
});

app.listen(3000);
