var express = require('express')
var app = express()
 
app.get('/blocks', function (req, res) {
 	var blocks= ['Fixed','Movable','Rotating'];
 	//res.send(blocks);
 	res.json(blocks);
});
 
app.listen(3000,function(){
	console.log('Listening to port 3000');
});	