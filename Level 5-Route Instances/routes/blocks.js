var express = require('express');
var router= express.Router(); // Retorna una instancia de router con la que se puede montar como un middleware

var bodyParser= require('body-parser'); //A new body object containing the parsed data is populated on the request object after the middleware (i.e. req.body).
var parseUrlencoded= bodyParser.urlencoded({extended:false}); //Forza el uso la libreria node nativa 'querystring', 

var blocks={
	'Fixed':'Fastened securely in position',
	'Movable': 'Capable of being moved',
	'Rotating':'Moving in a circle around'
};


//El root path relative se coloca el path donde fue montado, en este caso
// app.use('/blocks',...); que esta en el server.js

router.route('/')

.get(function(request, response){
	response.json(Object.keys(blocks));
})

.post(parseUrlencoded,function(request,response){
	//Las rutas pueden tener 'múltiples controladores' como argumentos y deben ser llamados secuencialmente
	//Agrego ruta para POST blocks. Primero se ejecuta parseUrlencoded y luego la funcion anonima
	//Poder manejar multiples rutas es util para re-utilizar middleware que carge recursos, produzca validaciones
	// autentifacion, etc

	//La informacion contenidad en el 'Form' puede ser accesida a traves de request.body
	var newBlock= request.body;  //retorna la data en Form
	blocks[newBlock.name]= newBlock.description; //Se crea nuevo bloque

	response.status(201).json(newBlock.name);  //Se responde con el nombre del nuevo block y status 201
});


router.route('/:name')

//Utilizo middleware .all para recibir todas las direcciones y hacer lo que hacia esta funcion antes
//app.param('name', function(request, response, next){});
//Como es middleware, siempre se ejecuta ALL y luego GET o POST.
.all(function(request,response,next) {
	var name= request.params.name;
	var block= name[0].toUpperCase()+ name.slice(1).toLowerCase();

	request.blockName=block; //Este valor puede ser accedido por otras routes en la aplicacion
	next(); //Paso para continuar el request

})

.get(function(request, response){
	var description= blocks[request.blockName];  //Ahora puedo acceder directamente

	if(!description){
		response.status(404).json('No description found for '+ request.blockName);
	}
	else{
		response.json(description);
	}
})

.delete(function(request,response){
	if(blocks[request.blockName]){
		delete blocks[request.blockName];  //que corresponde a app.param('name',...)
		response.sendStatus(200);
	}
	else
		response.sendStatus(404);
	});


/*********************EXPORTO COMO MODULO*************************************/
module.exports = router;