//Se puede refactorizar las rutas utilizando 'route'
//Las rutas pueden tener 'múltiples controladres'como argumentos y deben ser llamados secuencialmente

var express = require('express');
var app = express();


app.use(express.static('public-route-router'));

//El router es un module Node que esta en el archivo ./routes/blocks.js
var blocks= require('./routes/blocks');

//Monto el router a un root url como '/blocks' 
app.use('/blocks',blocks);

app.listen(3000);	