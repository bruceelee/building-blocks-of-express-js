$(function(){
	
	$.get('/blocks',appendToList);

	$('form').on('submit',function(event){
		event.preventDefault(); 		 //Impide que inmediatamente se haga un submit
		var form= $(this);  			 //Se asigna para manejarlo
		var blockData=form.serialize();  //Serialize transforma la data a notacion URL

		$.ajax({						//Lo enviamos con metodo POST
			type:'POST', url:'/blocks', data:blockData
		}).done(function(blockName){    //Cuando finalize el POST, se recibe el nombre del block
			appendToList([blockName]);  //Colocamos el nuevo block dentro de la lista que se posee
			form.trigger('reset');  	//Se limpia los campos del form
		});

	});
	function appendToList(blocks){
		var list= [];
		var content, block;

		for(var i in blocks){
			block= blocks[i];
			content='<a href="/blocks/'+block+'">'+block+'</a>';
			list.push($('<li>',{ text:blocks[i] }));
		}
		$('.block-list').append(list);
	}
	
//DELETE
	$('.block-list').on('click','a[data-block]',function(event){
		if(!confirm('Estas seguro?')){
		}

		var target= $(event.currentTarget);

		$.ajax({
			type:'DELETE', url: '/blocks/' + target.data('block')
		}).done(function(){
			target.parents('li').remove();
		});
	});
});